// console.log("Test");

	let getCube = (num) => Math.pow(num, 3);
	let numToGetCube = 3;
	let cubeOfNum = getCube(numToGetCube);
	console.log(`The cube of ${numToGetCube} is ${cubeOfNum}`)

	let address = ["1387", "Cordero", "Lambakin", "Marilao", "Bulacan"]
	let [houseNumber, street, barangay, city, province] = address;
	console.log(`I live at ${houseNumber} ${street}, ${barangay}, ${city}, ${province}`);

	let animal = {
		name: "Lolong",
		species: "saltwater",
		weight: "1075",
		measurement: "30 ft 3 in"
	};


	let { name, species, weight, measurement } = animal;
	console.log(`${name} was a ${species} crocodile. He weighed at ${weight} kgs with a measurement of ${measurement}`);


	let arrNum = [3, 6, 9, 12, 15];
	// let sum = 0;
	arrNum.forEach(num => {
		console.log(num);
		// sum = sum + num;	
	})
	// console.log(sum);

	let reduceNumber = () => {
		return arrNum.reduce((x,y) => x + y);	
	}
	
	console.log(reduceNumber());

	class Dog{
		constructor(name, age, breed){
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
	}

	let dog1 = new Dog("Frankie", 5, "Miniature Daschschund");
	console.log(dog1);
	



